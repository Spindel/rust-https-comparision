use std::path::Path;

fn make_clientconfig() -> reqwest::Certificate {
    let ca_path = Path::new("ca.modio.se.cacert");
    let ca_data = std::fs::read(&ca_path).unwrap();
    let cert = reqwest::Certificate::from_pem(&ca_data).expect("Error making reqwest certificate");
    cert
}

fn fetch_https() -> Result<(), ()> {
    let res =
        reqwest::blocking::get("https://ca.sommar.modio.se/root.crt").expect("failed request");
    match res.status().as_u16() {
        200 => Ok(()),
        _ => Err(()),
    }
}

fn fetch_private() -> Result<(), ()> {
    let cert = make_clientconfig();
    let client = reqwest::blocking::Client::builder()
        .add_root_certificate(cert)
        .build()
        .expect("Error making client");
    let res = client
        .get("https://ca.modio.se/root.crt")
        .send()
        .expect("error fetching from private tls");
    match res.status().as_u16() {
        200 => Ok(()),
        _ => Err(()),
    }
}

fn main() {
    fetch_https().expect("Public failed");
    fetch_private().expect("Private failed");
    println!("OK");
}
