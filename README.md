# Rust https comparision

Q: What is this? 
A: Small examples of https connections for various rust libraries

Q: Why?
A: I wanted to compare them

Q: Why?
A: Because I needed to.

Q: But really?
A: I needed to do simple, syncronous, HTTPS requests using both a private PKI and public ones.

# Requirements

1. Statically linked binary
2. Cross compile-able
3. HTTPS with public CA certificates
4. HTTPS with only private CA certificate
5. HTTP 1.1 required.
6. TLS 1.2 required

# Extras

1. TLS 1.3 is gravy
2. HTTP 2 is gravy
3. JSON parsing is gravy


# Clients implemented:

- ureq
- curl
- curl-mesa ( curl with mesalink instead of openssl)
- attohttpc
- reqwest

# Disqualified:

- minreq , no option to use CA certificates


# Compiled binary sizes (nightly)

| implementation |release + lto | stripped | 
|----------------|--------------|----------|
|attohttpc	| 5 770 976     |3 953 280 |
|curl		| 4 427 776     |1 887 632 |
|curl-mesa	| 4 427 784     |1 887 632 |
|reqwest	| 9 161 776     |6 067 336 |
|ureq		| 4 209 272     |1 735 912 |

# Remarks

Several libraries are still using the system CA certificate stores rather than
built-in ones.
The only one that never touches the CA store is `ureq`.
