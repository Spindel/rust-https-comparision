use curl::easy::Easy;
use std::path::Path;

fn fetch_https() -> Result<(), ()> {
    let mut handle = Easy::new();
    handle.url("https://ca.sommar.modio.se/root.crt").unwrap();
    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|from_server| Ok(from_server.len()))
            .unwrap();
        transfer.perform().unwrap();
    }
    let status_code = handle.response_code().unwrap();
    if status_code == 200 {
        Ok(())
    } else {
        Err(())
    }
}

fn fetch_internal() -> Result<(), ()> {
    let ca_path = Path::new("ca.modio.se.cacert");
    let mut handle = Easy::new();
    handle.url("https://ca.modio.se/root.crt").unwrap();
    handle.cainfo(ca_path).unwrap();
    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|from_server| Ok(from_server.len()))
            .unwrap();
        transfer.perform().unwrap();
    }
    let status_code = handle.response_code().unwrap();
    if status_code == 200 {
        Ok(())
    } else {
        Err(())
    }
}
/*
fn fetch_should_fail() -> Result<(), ()> {
    fn inner() -> Result<u32, curl::Error> {
        let mut handle = Easy::new();
        handle.url("https://ca.modio.se/root.crt")?;
        {
            let mut transfer = handle.transfer();
            transfer.write_function(|from_server| {
                Ok(from_server.len())
            })?;
            transfer.perform()?;
        }
        let status = handle.response_code()?;
        Ok(status)
    }
    match inner() {
        Ok(200) => Ok(()),
        Ok(_) => Err(()),
        Err(_) => Err(()),
    }
}
*/

fn main() {
    fetch_https().expect("Public failed");
    fetch_internal().expect("HTTPS with private PKI failed");
    println!("OK");
}
