use std::path::Path;

fn make_clientconfig() -> rustls::ClientConfig {
    let ca_path = Path::new("ca.modio.se.cacert");
    let ca_data = std::fs::read(&ca_path).unwrap();
    let mut cursor = std::io::Cursor::new(ca_data);
    let mut tls_config = rustls::ClientConfig::new();
    let (_added, _useless) = tls_config
        .root_store
        .add_pem_file(&mut cursor)
        .expect("Error adding cert");
    tls_config
}

fn fetch_https() -> Result<(), ()> {
    let resp = attohttpc::get("https://ca.sommar.modio.se/root.crt")
        .send()
        .expect("Error doing GET request");
    match resp.status().as_u16() {
        200 => Ok(()),
        _ => Err(()),
    }
}

fn fetch_private() -> Result<(), ()> {
    let tls_config = make_clientconfig();
    let resp = attohttpc::get("https://ca.sommar.modio.se/root.crt")
        .client_config(tls_config)
        .send()
        .expect("Error doing GET request");
    match resp.status().as_u16() {
        200 => Ok(()),
        _ => Err(()),
    }
}
fn main() {
    fetch_https().expect("Public failed");
    fetch_private().expect("Private failed");
    println!("OK");
}
