use std::path::Path;

fn make_clientconfig() -> rustls::ClientConfig {
    let ca_path = Path::new("ca.modio.se.cacert");
    let ca_data = std::fs::read(&ca_path).unwrap();
    let mut cursor = std::io::Cursor::new(ca_data);
    let mut tls_config = rustls::ClientConfig::new();
    let (_added, _useless) = tls_config
        .root_store
        .add_pem_file(&mut cursor)
        .expect("Error adding cert");
    tls_config
}

fn fetch_https() -> Result<(), ()> {
    let resp = ureq::get("https://ca.sommar.modio.se/root.crt").call();
    match resp.status() {
        200 => Ok(()),
        _ => Err(()),
    }
}

fn fetch_private() -> Result<(), ()> {
    let resp = ureq::get("https://ca.modio.se/root.crt")
        .set_tls_config(std::sync::Arc::new(make_clientconfig()))
        .call();
    match resp.status() {
        200 => Ok(()),
        _ => Err(()),
    }
}
fn main() {
    fetch_https().expect("Public failed");
    fetch_private().expect("Private failed");
    println!("OK");
}
